python-pysnmp2 (2.0.9-4) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Drop obsolete Conflicts/Replaces with python2.3-pysnmp2 and
    python2.4-pysnmp2.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:06:56 -0500

python-pysnmp2 (2.0.9-3) unstable; urgency=medium

  * Team upload.
  * Move Python modules back into the pysnmp.v2 namespace.
  * Depend on python-pysnmp-common (>= 4.1.9a-2), so that both packages use
    the same directory layout (closes: #609453).

 -- Jakub Wilk <jwilk@debian.org>  Wed, 12 Jan 2011 00:08:04 +0100

python-pysnmp2 (2.0.9-2) unstable; urgency=medium

  [ Piotr Ożarowski ]
  * Vcs-Svn, Vcs-Browser and Homepage fields added

  [ Sandro Tosi ]
  * debian/control
    - uniforming Vcs-Browser field
    - switch Vcs-Browser field to viewsvn
    - bump Standards-Version to 3.8.1 (no changes needed)
    - bump cdbs versioned b-d
    - added ${misc:Depends} to bin package Depends
  * debian/rules
    - replace DEB_PYTHON_MODULE_PACKAGE with DEB_PYTHON_MODULE_PACKAGES; thanks
      to Josselin Mouette for the bug report; Closes: #517209

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Fri, 10 Apr 2009 14:52:41 +0200

python-pysnmp2 (2.0.9-1) unstable; urgency=low

  * New upstream release

 -- Jan Luebbe <jluebbe@lasnet.de>  Fri, 30 Jun 2006 17:40:24 +0200

python-pysnmp2 (2.0.8-2) unstable; urgency=low

  [ Jan Luebbe ]
  * Use python-support and drop version-specific packages.
  * Install into pysnmp/v2 for use with pysnmp version selection.
  * Document these changes in README.Debian.

  [ Arnaud Fontaine ]
  * Update Standards-Version to 3.7.2.
  * Add Homepage field into debian/control.in.
  * Add myself and debian-python team to uploaders.
  * Add python-pysnmp-common to Depends.
  * New Python policy changes. Closes: #373345.
    + debian/compat:
      - Update debhelper compatibility to 5.
    + debian/rules:
      - Add DEB_PYTHON_SYSTEM=pysupport.
    + debian/control.in
      - Add XS-Python-Version to source package.
      - Clean Build-Depends and Depends, cdbs handles that
        automatically now.
      - XB-Python-Version and Provides for binary package.
    + debian/postinst && debian/prerm:
      - Remove "hand made" python-support stuff, cdbs does this.

 -- Arnaud Fontaine <arnaud@andesi.org>  Tue, 20 Jun 2006 12:41:20 +0200

python-pysnmp2 (2.0.8-1) unstable; urgency=low

  * New upstream release (closes: #324608)

 -- Jan Luebbe <jluebbe@lasnet.de>  Tue, 23 Aug 2005 13:02:00 +0200

python-pysnmp2 (2.0.5-1) unstable; urgency=low

  * Initial Release.
  * Uploading to Debian (closes: #140442)

 -- Jan Luebbe <jluebbe@lasnet.de>  Thu,  7 Jul 2005 16:42:50 +0200
